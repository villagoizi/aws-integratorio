'use strict'
// eslint-disable-next-line import/no-extraneous-dependencies
const { v4 } = require('uuid')
const { CreatePurchaseValidations } = require('../schema/input/createPurchase.validation');
const { GetClientByIdCommand } = require('../schema/command/getClientById');
const { requestGetClientById } = require('../service/requestClientById.service');
const { requestUpdateClient } = require('../service/requestUpdateClient.service');
const { UpdateClientCommand } = require('../schema/command/updateClient');
const { createPurchaseService } = require('../service/createPurchase.service');

module.exports = async (commandPayload, commandMeta) => {
    new CreatePurchaseValidations(commandPayload, commandMeta);
    const { dni, products } = commandPayload
    const found = await requestGetClientById(new GetClientByIdCommand({ dni }, commandMeta))
    if (found?.active === 'DELETED') return { status: 401, body: { message: 'Client is inactive' } }
    const percentApply = found.creditCard.type === 'Gold' ? 0.12 : 0.08;
    // eslint-disable-next-line no-unused-vars
    let purchaseScore = found.purchaseScore ?? 0;
    const purchaseProducts = products.map(({ name, price }) => {
        const finalPrice = price - (percentApply * price);
        if (finalPrice >= 200) { purchaseScore += 1 }
        return {
            name,
            price,
            finalPrice
        }
    });
    await requestUpdateClient(new UpdateClientCommand({ purchaseScore, dni }, commandMeta));
    const result = await createPurchaseService({id: v4(), products: purchaseProducts, dni});
    return { body: result }
}