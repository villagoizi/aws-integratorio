'use strict'
const clientSchema = {
    strict: false,
    name: { type: String, required: false },
    lastName: { type: String, required: false },
    dni: { type: String, required: false },
    gift: { type: String, required: false },
    birth: { type: String, required: false },
    active: { type: String, required: false },
    creditCard: { type: Object, required: false },
    purchaseScore: { type: Number, required: false }
}

module.exports = { clientSchema }