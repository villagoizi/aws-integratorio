'use strict'
const { DownstreamCommand } = require('nbased/schema/downstreamCommand');
const { clientSchema } = require('./commonSchema');

class GetClientByIdCommand extends DownstreamCommand {
  constructor(payload, meta) {
    super({
      type: 'GET_CLIENT_BY_ID_COMMAND',
      payload,
      meta,
      requestSchema: {
        schema: {
          dni: { type: String, required: true },
        }
      },
      responseSchema: {
        schema: {
          ...clientSchema,
          creditCard: { type: Object, required: true }
        }
      }
    })
  }
}

module.exports = { GetClientByIdCommand };