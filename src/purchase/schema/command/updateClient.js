'use strict'
const { DownstreamCommand } = require('nbased/schema/downstreamCommand');
const { clientSchema } = require('./commonSchema');

class UpdateClientCommand extends DownstreamCommand {
    constructor(payload, meta) {
        super({
            type: 'UPDATE_CLIENT_COMMAND',
            payload,
            meta,
            requestSchema: {
                schema: {
                    purchaseScore: { type: Number, required: true },
                    dni: { type: String, required: true }
                }
            },
            responseSchema: {
                schema: clientSchema
            }
        })
    }
}

module.exports = { UpdateClientCommand };