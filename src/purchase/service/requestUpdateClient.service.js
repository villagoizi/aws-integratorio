'use strict'
const request = require('nbased/service/downstream/request');
const config = require('nbased/util/config');
const { ErrorHandled } = require('nbased/util/error');

const API_URL = config.get('API_CLIENTS_URL')

const requestUpdateClient = async (updateClientCommand) => {
    const { commandMeta, commandPayload } = updateClientCommand.get();
    const { dni, purchaseScore } = commandPayload
    const url = `${API_URL}/clients/${dni}`;


    const requestParams = {
        url,
        method: 'PUT',
        timeout: 1000 * 10,
        data: { purchaseScore }
    }
    const response = await request(requestParams, commandMeta).catch(error => {
        if (error instanceof ErrorHandled) {
            const { error: responseError } = error.message;
            const code = (responseError.includes('not supported')) ? 'INVALID_BASE_ERROR' : null;
            updateClientCommand.getErrorCataloged(code, responseError);
        }
        throw error;
    });
    updateClientCommand.validateResponse(response);
    return response;
}
module.exports = { requestUpdateClient }