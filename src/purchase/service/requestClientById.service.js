'use strict'
const request = require('nbased/service/downstream/request');
const config = require('nbased/util/config');
const { ErrorHandled } = require('nbased/util/error');

const API_URL = config.get('API_CLIENTS_URL')

const requestGetClientById = async (getClientByIdCommand) => {
  const { commandMeta, commandPayload } = getClientByIdCommand.get();
  const url = `${API_URL}/clients/${commandPayload.dni}`;

  const requestParams = {
    url,
    method: 'GET',
    timeout: 1000 * 10,
    params: commandPayload
  }
  // eslint-disable-next-line no-console, no-undef
  console.log(url); console.log(requestParams);
  const response = await request(requestParams, commandMeta).catch(error => {
    if (error instanceof ErrorHandled) {
      const { error: responseError } = error.message;
      const code = (responseError.includes('not supported')) ? 'INVALID_BASE_ERROR' : null;
      getClientByIdCommand.getErrorCataloged(code, responseError);
    }
    throw error;
  });
  getClientByIdCommand.validateResponse(response);
  return response;
}
module.exports = { requestGetClientById }