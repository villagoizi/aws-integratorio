'use strict'
const dynamo = require('nbased/service/storage/dynamo');
const config = require('nbased/util/config');

const TABLE_NAME = config.get('PURCHASES_TABLE');

const createPurchaseService = async (Item) => {
    const resp = await dynamo.putItem({ Item, TableName: TABLE_NAME })
    return resp.Item;
}

module.exports = { createPurchaseService }
