'use strict'
const { CreateClientEventValidation } = require('../schema/input/createClientEvent.validation');
const { updateClientService } = require('../service/updateClient.service');
const { giftChooser } = require('../helper/gift-choser');

module.exports = async (payload, meta) => {
    const validations  = new CreateClientEventValidation(JSON.parse(payload.Message), meta, 'CLIENT.CREATE_CARD')
    const schema = validations.get();
    const gift = giftChooser(schema.birth);
    const params = {
        ExpressionAttributeNames: {
            '#G': 'gift',
        },
        ExpressionAttributeValues: {
            ':g': gift
        },
        Key: {
            dni: schema.dni
        },
        ReturnValues: 'ALL_NEW',
        UpdateExpression: 'SET #G = :g',
    }
    await updateClientService(params);
}