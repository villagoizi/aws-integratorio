'use strict'
const { updateItem } = require('nbased/service/storage/dynamo');
const { createUpdateExpression } = require('../helper/dynamo-update-parser');
const { GetClientByIdValidation } = require('../schema/input/getClientById.validation');
const { getClientById } = require('../service/getById.service');

module.exports = async (payload, meta) => {
    const validations = new GetClientByIdValidation(payload, meta);
    const client = await getClientById({
        Key: validations.get(),
    });
    if(!client?.Item) return { status: 404, body: { mesasge: 'Client not found'}}
    await updateItem({
        Key: validations.get(),
        ...createUpdateExpression({status: 'DELETED'}),
    })
    return {
        status: 200,
        body: { message: 'Delete client succesfully'}
    }
}