'use strict'
const { getAllClients } = require('../service/getAll.service');

module.exports = async () => {
    const items = await getAllClients();
    return {
        status: 200,
        body: { items: items ?? []}
    } 
}