'use strict'
const { CreateClientEventValidation } = require('../schema/input/createClientEvent.validation');
const { randomNumber } = require('../helper/random-number');
const { calculateAge } = require('../helper/age.utils');
const { updateClientService } = require('../service/updateClient.service');

module.exports = async (payload, meta) => {
    const validations  = new CreateClientEventValidation(JSON.parse(payload.Message), meta, 'CLIENT.CREATE_CARD')
    const schema = validations.get();
    const cardProperties = createCard(schema.birth);
    const params = {
        ExpressionAttributeNames: {
            '#C': 'creditCard',
        },
        ExpressionAttributeValues: {
            ':c': { ...cardProperties }
        },
        Key: {
            dni: schema.dni
        },
        ReturnValues: 'ALL_NEW',
        UpdateExpression: 'SET #C = :c',
    }
    await updateClientService(params);
}

const createCard = (birth) => ({
    number: `${randomNumber(0, 9999)}-${randomNumber(0, 9999)}-${randomNumber(0, 9999)}-${randomNumber(0, 9999)}`,
    type: calculateAge(birth) > 45 ? 'Gold' : 'Classic',
    expiration: `${randomNumber(1, 12)}/${randomNumber(21, 35)}`,
    ccv: `${randomNumber(0, 999)}`
});