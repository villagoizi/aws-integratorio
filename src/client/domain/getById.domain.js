'use strict'
const { GetClientByIdValidation } = require('../schema/input/getClientById.validation');
const { getClientById } = require('../service/getById.service');

module.exports = async (payload, meta) => {
    const validations = new GetClientByIdValidation(payload, meta);
    const client = await getClientById({
        Key: validations.get(),
    });
    if(!client?.Item) return { status: 404, body: { mesasge: 'Client not found'}}
    return {
        status: 200,
        body: client.Item
    }
}