'use strict'
const { createUpdateExpression } = require('../helper/dynamo-update-parser');
const { ClientCreatedEvent } = require('../schema/event/clientCreated');
const { UpdateClientValidation } = require('../schema/input/updateClient.validation');
const { emitClientCreated } = require('../service/emitClientCreated');
const { getClientById } = require('../service/getById.service');
const { updateClientService } = require('../service/updateClient.service');

module.exports = async (commandPayload, commandMeta, rawCommand) => {
    const { dni: payloadDni, ...payload } = commandPayload
    const dni = rawCommand?.pathParameters?.dni ?? payloadDni 
    const validations = new UpdateClientValidation(payload, commandMeta);
    if (!dni) return { status: 400, body: { message: 'Path {dni} is required' } };
    const current = await getClientById({ Key: { dni } });
    if (!current.Item) return { status: 404, body: { message: 'Client not found ' } }
    const schema = validations.get();
    if (!Object.keys(schema).length) return { status: 200, body: current.Item }
    const params = { ...createUpdateExpression(schema), Key: { dni }, ReturnValues: 'ALL_NEW' };
    const result = await updateClientService(params);
    if (schema?.birth && schema?.birth !== current?.birth) {
        const paramsEmit = { ...current.Item, ...schema }
        delete paramsEmit.gift;
        delete paramsEmit.creditCard;
        await emitClientCreated(new ClientCreatedEvent(paramsEmit, commandMeta));
    }
    return { body: result.Attributes }
}