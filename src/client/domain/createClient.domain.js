'use strict'
const { ClientCreatedEvent } = require('../schema/event/clientCreated');
const { CreateClientValidation } = require('../schema/input/createClientValidation');
const { createClientService } = require('../service/createClient.service');
const { emitClientCreated } = require('../service/emitClientCreated');

module.exports = async (commandPayload, commandMeta) => {
    const validations = new CreateClientValidation(commandPayload, commandMeta);
    const schema = validations.get();
    const item = await createClientService(schema);
    await emitClientCreated(new ClientCreatedEvent(schema, commandMeta));
    return { body: item }
}