'use strict'
module.exports = { randomNumber: (minimum, maximum) => Math.round(Math.random() * (maximum - minimum) + minimum) }