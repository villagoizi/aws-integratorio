'use strict'
const calculateAge = (birthday) => {
    const birthDate = new Date(birthday)// birthday is a string in format YYYYMMDD
    const ageDifMs = Date.now() - birthDate.getTime();
    const ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

const isValidBirth = (date) => {
    const dateReceived = new Date(date);
    if(dateReceived == 'Invalid Date') return 'Invalid format to field birth must be YYYY-MM-DD';
    const currentAge = calculateAge(date);
    if(currentAge < 18 || currentAge > 65) return 'Client must be between 65 and 18 years old'
    return true;
}

module.exports = { calculateAge, isValidBirth }