'use strict'
const DEFAULT_CONSTRAINTS = {
    MAJOR: { gt: 20 },
    MINOR: { lt: 20 }
}

const GIFTS = {
    '12-1-2-3': {
        value: 'Remera',
        constraint: { 12: DEFAULT_CONSTRAINTS.MAJOR, 3: DEFAULT_CONSTRAINTS.MINOR }
    },
    '3-4-5-6': {
        value: 'Buzo',
        constraint: { 3: DEFAULT_CONSTRAINTS.MAJOR, 6: DEFAULT_CONSTRAINTS.MINOR }
    },
    '6-7-8-9': {
        value: 'Sweater',
        constraint: { 6: DEFAULT_CONSTRAINTS.MAJOR, 9: DEFAULT_CONSTRAINTS.MINOR }
    },
    '9-10-11-12': {
        value: 'Camisa',
        constraint: { 9: DEFAULT_CONSTRAINTS.MAJOR, 12: DEFAULT_CONSTRAINTS.MINOR }
    },
}
const ASSIGNAMENTS_GIFT = Object.entries(GIFTS);

exports.giftChooser = (date, divider = '-') => {
    const [, month, day] = date.split(divider);
    let result;
    for (const values of ASSIGNAMENTS_GIFT) {
        const ranges = values[0].split('-').map(v => +v);
        if (!ranges.includes(+month)) continue;
        const { constraint, value } = values[1];
        const kConstraint = `${+month}`;
        if (!constraint[kConstraint]) {
            result = value;
            break;
        }
        const validate = constraint[kConstraint];
        if (+day <= validate?.lt) {
            result = value;
            break;
        }
        if (+day >= validate?.gt) {
            result = value;
            break;
        }
    }

    return result;

}