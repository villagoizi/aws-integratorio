'use strict'
const createUpdateExpression = (params) => {
  const paramsKeys = Object.keys(params);
  let UpdateExpression = 'set ';
  const update = paramsKeys.reduce(
    (acc, v, i) => {
      const ExpressionAttributeValuesKey = `:${v}`;
      const ExpressionAttributeNamesKey = `#${v}`;
      acc.ExpressionAttributeNames = {
        ...acc.ExpressionAttributeNames,
        [ExpressionAttributeNamesKey]: v,
      };
      acc.ExpressionAttributeValues = {
        ...acc.ExpressionAttributeValues,
        [ExpressionAttributeValuesKey]: params[v],
      };
      UpdateExpression += `${ExpressionAttributeNamesKey} = ${ExpressionAttributeValuesKey}`;
      if (i !== paramsKeys.length - 1) {
        UpdateExpression += ',';
      }
      return acc;
    }, {}
  );
  return {
    ...update,
    UpdateExpression,
  }
}

module.exports = { createUpdateExpression }