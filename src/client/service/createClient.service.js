'use strict'
const dynamo = require('nbased/service/storage/dynamo');
const config = require('nbased/util/config');

const TABLE_NAME = config.get('CLIENTS_TABLE');

module.exports.createClientService = async (Item) => {
    const resp = await dynamo.putItem({ Item, TableName: TABLE_NAME })
    return resp.Item;
}
