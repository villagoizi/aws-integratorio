'use strict'
const dynamo = require('nbased/service/storage/dynamo');
const config = require('nbased/util/config');

const TABLE_NAME = config.get('CLIENTS_TABLE');

module.exports.getAllClients = async () => {
    const resp = await dynamo.scanTable({ TableName: TABLE_NAME })
    return resp.Items;
}
