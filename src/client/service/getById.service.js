'use strict'
const dynamo = require('nbased/service/storage/dynamo');
const config = require('nbased/util/config');

const TABLE_NAME = config.get('CLIENTS_TABLE');

module.exports.getClientById = (params) => dynamo.getItem({ TableName: TABLE_NAME, ...params })