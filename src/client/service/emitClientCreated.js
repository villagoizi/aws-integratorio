'use strict'
const sns = require('nbased/service/downstream/sns');
const config = require('nbased/util/config');

const CLIENT_CREATED_TOPIC = config.get('CLIENTS_CREATED_TOPIC');

const emitClientCreated = async (clientCreatedEvent) => {
    const { eventPayload, eventMeta } = clientCreatedEvent.get();
    const params = {
        TopicArn: CLIENT_CREATED_TOPIC,
        Message: eventPayload
    };
    await sns.publish(params, eventMeta);
}

module.exports = { emitClientCreated }