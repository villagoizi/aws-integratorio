'use strict'
const { batchEventMapper } = require('nbased/handler')
const inputMode = require('nbased/handler/input/batchEventQueue');
const outputMode = require('nbased/handler/output/batchEventConfirmation');
const domain = require('../domain/createCard.domain');

module.exports.handler = async (events, context) => {
  return batchEventMapper({ events, context }, inputMode, domain, outputMode);
}