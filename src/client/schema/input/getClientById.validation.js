'use strict'
const { InputValidation } = require('nbased/schema/inputValidation');


const schema = {
    dni: { type: String, required: true }
}
class GetClientByIdValidation extends InputValidation {
    constructor(payload, meta) {
        super({
            type: 'CLIENT.GET_CLIENT_BY_ID',
            specversion: 'v1.0.0',
            payload,
            source: meta?.source,
            inputSchema: { schema }
        })
    }
}

module.exports = { GetClientByIdValidation }