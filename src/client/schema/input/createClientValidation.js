'use strict'
const { InputValidation } = require('nbased/schema/inputValidation');
const { isValidBirth } = require('../../helper/age.utils');


const schema = {
    dni: { type: String, required: true },
    name: { type: String, required: true },
    lastName: { type: String, required: true },
    birth: {
        type: String, required: true, custom: isValidBirth
    }
}
class CreateClientValidation extends InputValidation {
    constructor(payload, meta) {
        super({
            type: 'CLIENT.CREATE_CLIENT',
            specversion: 'v1.0.0',
            payload,
            source: meta?.source,
            inputSchema: { schema }
        })
    }
}

module.exports = { CreateClientValidation }