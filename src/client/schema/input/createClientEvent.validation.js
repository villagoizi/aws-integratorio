'use strict'
const { InputValidation } = require('nbased/schema/inputValidation');
const { isValidBirth } = require('../../helper/age.utils');


const schema = {
    strict: false,
    name: {type: String, required: false},
    lastName: {type: String, required: false},
    dni: { type: String, required: true },
    birth: {
        type: String, required: true, custom: isValidBirth
    }
}
class CreateClientEventValidation extends InputValidation {
    constructor(payload, meta, type) {
        super({
            type,
            specversion: 'v1.0.0',
            payload,
            source: meta?.source,
            inputSchema: { schema }
        })
    }
}

module.exports = { CreateClientEventValidation }