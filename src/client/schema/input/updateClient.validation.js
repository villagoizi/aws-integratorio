'use strict'
const { InputValidation } = require('nbased/schema/inputValidation');
const { isValidBirth } = require('../../helper/age.utils');

const schema = {
    name: { type: String, required: false },
    lastName: { type: String, required: false },
    birth: {
        type: String, required: false, custom: (value) => value ? isValidBirth(value) : true
    },
    purchaseScore: { type: Number, required: false }
}
class UpdateClientValidation extends InputValidation {
    constructor(payload, meta) {
        super({
            type: 'CLIENT.UPDATE_CLIENT',
            specversion: 'v1.0.0',
            payload,
            source: meta?.source,
            inputSchema: { schema }
        })
    }
}

module.exports = { UpdateClientValidation }