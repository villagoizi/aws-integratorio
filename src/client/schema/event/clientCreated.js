'use strict'
const { DownstreamEvent } = require('nbased/schema/downstreamEvent');

const schema = {
    strict: false,
    dni: { type: String, required: true },
    name: { type: String, required: true },
    lastName: { type: String, required: true },
    birth: { type: String, required: true }
}

class ClientCreatedEvent extends DownstreamEvent {
    constructor(payload, meta) {
        super({
            type: 'CLIENT.CLIENT_CREATED',
            specversion: 'v1.0.0',
            payload,
            meta,
            eventSchema: { schema }
        })
    }
}

module.exports = { ClientCreatedEvent } 